#pragma once

#include <cstdlib>
#include "cache_manager.h"

struct Node;

struct NodeRef {
public:
  NodeRef(off_t offset, CacheManager* cm) : offset_(offset), cache_man_(cm) {}
  ~NodeRef() {}

  Node* get() {
    return fetch<Node*>(offset(), *cache_man_);
  }

  off_t offset() const {
    return offset_;
  }

  static NodeRef null_ref() {
    static const NodeRef null_node_ref = NodeRef{-1, nullptr};
    return null_node_ref;
  }

private:
  off_t offset_;
  CacheManager* cache_man_;
};


struct Node {
public:
  Node(int data, const NodeRef& left, const NodeRef& right)
      : data_(data), left_(left), right_(right) {}

  NodeRef left() const {
    return left_;
  }

  NodeRef right() const {
    return right_;
  }

  int data() const {
    return data_;
  }
private:
  int     data_;
  NodeRef left_;
  NodeRef right_;
};



