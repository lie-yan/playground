#pragma once

#include <cstddef>

class Blob {
public:
  Blob(char * buf, size_t nbyte)
      : buffer_(buf), nbyte_(nbyte) {}

  char* get() {
    return buffer_;
  }

  size_t size() const {
    return nbyte_;
  }

private:
  char * buffer_;
  size_t nbyte_;
};
