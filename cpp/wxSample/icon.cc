//
// Created by robin on 12/30/15.
//

#include "Icon.h"


Icon::Icon(const wxString &title)
  : wxFrame(nullptr, wxID_ANY, title, wxDefaultPosition, wxSize(250, 150)) {

  this->Connect(wxEVT_PAINT, wxPaintEventHandler(Icon::OnPaint));
  this->SetIcon(wxIcon(wxT("web.xpm")));
  this->Centre();
}

void Icon::OnPaint(wxPaintEvent &event) {
  wxPaintDC dc(this);

  wxRect rect = GetClientRect();

  if (rect.width == 0 or rect.height==0)
    return;

  CGContextRef  context = (CGContextRef) dc.GetGraphicsContext()->GetNativeContext();
  if (context == 0)
    return;

  Cairo::RefPtr<Cairo::QuartzSurface> cr_surface = Cairo::QuartzSurface::create(context,
                                                                                rect.width,
                                                                                rect.height);
  Cairo::RefPtr<Cairo::Context> cr = Cairo::Context::create(cr_surface);
  Render(cr, rect.width, rect.height);

  cr_surface->flush();

  CGContextFlush(context);

}



void Icon::Render(Cairo::RefPtr<Cairo::Context> cr, int width, int height) {

  cr->set_source_rgba(0.3, 0.4, 0.5, 0.8);
  cr->paint();

  cr->set_source_rgba(0.0, 0.0, 0.0, 0.7);
  cr->arc(width/2.0, height/2.0,
          height/4.0, 0.0, 2.0*M_PI);
  cr->stroke();
}
